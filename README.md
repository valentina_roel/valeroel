## Bienvenido a la web personal de Valentina Roel para la Especialización en Fabricación Digital e Innovación

Soy ingeniera agrónoma, recientemente recibida.

Siempre me apasionaron las ciencias aplicadas. Me interesa el desarrollo de nuevas tecnologías que se pueden utilizar de manera práctica, con el fin de colaborar al desarrollo local.

Hace click  **[acá](https://gitlab.com/valentina_roel/valeroel/-/tree/master)** para acceder a mi repositorio de GitLab

Este es el repositorio donde pondré toda la información del curso.

Si deseas comunicarte conmigo escribí al mail valentina.roel@estudiantes.utec.edu.uy
