---
hide:
    - toc
---

# <span style="color:green"> MP 01
## <span style="color:green"> Introducción a la Fabricación digital e Innovación

El desafío de este módulo consistía en identificar una posible idea de proyecto final que incorporara diseño 2D y 3D, procesos de fabricación aditivos y sustractivos, electrónica y programación. A su vez teníamos que seleccionar 3 o más proyectos y posibles ODS que se relacionaran con ella.

Como ing. agr. consideré que debía solucionar alguna problemática que estuviera ocurriendo en el agro uruguayo. Para eso me reuní con técnicos de INIA y juntos identificamos que las aves plaga estaban siendo un problema que podía solucionar.


![](../images/biblio.png){ width=50% }

Basándome en soluciones que existen para otras partes del mundo (como **[Ultrason](https://bird-x.com/bird-products/electronic/ultrasonic/ultrason-x/)** y **[Balcony Gard](https://bird-x.com/bird-products/electronic/ultrasonic/balcony-gard/)** ) decidí combinar los métodos ya existentes y generar una alternativa adaptada a las aves locales.

![](../images/idea_proyecto.png){ width=100% }
