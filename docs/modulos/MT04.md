---
hide:
    - toc
---

# <span style="color:green"> MT04
## <span style="color:green"> Modelado 3D

# Terminología importante

Una **impresora 3D** es una máquina que construye de forma automática un objeto tridimensional a partir de un fichero electrónico.
Las **herramientas CAD** (como **[Fusion360](https://www.autodesk.com/products/fusion-360/personal)** y  **[Rhinoceros®](y https://www.rhino3d.com))** son las que permiten modelar los objetos 3D en el ordenador.

Los objetos 3D para imprimir se pueden obtener de diseños ya realizados por otras personas que se encuentran en repositorios online disponibles, modelarlos nosotros mismos, o modelarlos automáticamente a través de un escáner 3D.


## MESH vs NURBS

**Modelo poligonal** son puntos en un espacio 3D, conectados para formar una malla poligonal.

**Modelado de curvas** las superficies están definidas por curvas, las cuales son influenciadas por la ponderación del control de puntos.

***

### Consigna

**La consigna de este modulo consistió en:**

*Diseñar y modelar en 3D un elemento que sea de utilidad para el proyecto final en el software paramétrico FUSION 360. Documentar los distintos pasos del proceso en tu página web.*

*Las piezas deben tener un tamaño máximo de 20X20X20 cm.*

El diseño se realizó en **[Fusion360](https://www.autodesk.com/products/fusion-360/personal)** tal como indica la consigna.
Primero descargué el programa y me registré con la licencia de estudiante.

<iframe width="950" height="534" src="https://www.youtube.com/embed/sZwM87-nsYA" title="Interface de usuario" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Este video fue de gran utilidad para descubrir las utilidades del programa.

En mi caso utilicé un modelo del repositorio de Fusion360.

![](../images/MT04/repositorio.png){ width=50% }

El objeto  consistió en una caja para contener la electrónica del proyecto.

![](../images/MT04/caja.png){ width=50% }


A esta caja se le agregó el isotipo creado en el modulo MT02 en formato SVG.

![](../images/MT04/isotipo.png){ width=30% }

Luego con la herramienta extruir se le dio 2,5 mm de espesor para obtener el relieve del isotipo hacia arriba  y -0,5mm para combinar el isotipo con la tapa de la caja.

![](../images/MT04/extrucion.png){ width=40% }

Obteniendo así el siguiente objeto final:

![](../images/MT04/final.png){ width=50% }

[Aquí](https://gitlab.com/valentina_roel/valeroel/-/raw/master/docs/files/caja%20con%20isotipo%20v2.f3d) se puede descargar el Archivo .f3d realizado.

```

Autoevaluación: Al momento ninguna, se continúa trabajando en el modelo. Falta ajustar rosca de los tornillos
o decidir poner una tuerca del otro lado (solución más sencilla)

ValeTip: Preparar la tarea con más tiempo, no es una consigna difícil pero si requiere de mucho trabajo
y además Fusion360 tiene muchísimas herramientas para conocer.

```
***
