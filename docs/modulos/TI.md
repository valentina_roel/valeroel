---
hide:
    - toc
---

# <span style="color:green"> TI
## <span style="color:green"> Taller Integrador




## DESAFIO

- Montar una extrusor de pasta partiendo de una Anet A8

- Programar la placa de la Anet A8 con Arduino IDE

- Modelar e imprimir algo en 3D con materiales biodegradables

- Realizar una presentación del trabajo realizado en los tres días


###DESARMADO DE IMPRESORA

Después de conocer la Anet A8 reemplazamos el extrusor por uno soporte impreso en 3D donde se coloca la jeringa que ejercerá de extrusora. La jeringa es atravesada por una varilla roscada que al girar va presionando el contenido de la jeringa para que extruya.

![](../images/TI/desarme.png){ width=33% }![](../images/TI/desarme2.png){ width=33% }![](../images/TI/desarme3.png){ width=33% }

###EXPERIMENTACIÓN CON BIOMATERIALES 1: ARCILLA

Luego de probar distintas proporciones de agua/arcilla obtuvimos una que nos permitió probar imprimir sin que se derramara ni que por lo contrario fuera tan sólida que no le diera la fuerza para ser extruída.


<iframe src="https://player.vimeo.com/video/736006646?h=13385a342a&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="480" height="848" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="arcilla.mp4"></iframe>

En el vídeo se puede ver el momento exacto en el que se tapa con un grumo y esa pasada queda sin realizarse. A la siguiente vez que pasa y comienza a extruir como está a una distancia mayor que la que deberia de la capa que si fue impresa anteriormente, no logra pegarse y se comienza a desparramar. Logrando como resultado objetos planos y sin forma y no macetas.

###EXPERIMENTACIÓN CON BIOMATERIALES 2: CERÁMICA

Para la prueba con cerámica se siguieron los mismos pasos pero se realizaron más pruebas, variando la configuración de parámetros de impresión.

<iframe src="https://player.vimeo.com/video/736006666?h=9b25015838&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="480" height="848" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="ceramica.mp4"></iframe>

![](../images/TI/ceramica1.png){ width=33% }![](../images/TI/ceramica2.png){ width=33% }![](../images/TI/ceramica3.png){ width=33% }

![](../images/TI/ceramica4.png){ width=33% }![](../images/TI/ceramica5.png){ width=33% }![](../images/TI/ceramica6.png){ width=33% }

A partir del vídeo donde se puede escuchar la frustración por la impresión y de las imágenes con las distintas mezclas se concluye que lo primero y más importante es tener una mezcla buena y estandarizada. Luego de realizada se pueden variar los parámetros de impresión y obtener distintos resultados hasta llegar al ideal para esa mezcla en esas condiciones de ambiente.

###EXPERIMENTACIÓN CON BIOMATERIALES 3: CÁSCARA DE HUEVO

La cáscara de huevo quedó muy grande en la mezcla por lo tanto no pudo extruir bien. El resultado de impresión fue con Goma Xantica y la cáscara quedo dentro de la jeringa porque se separaron. RECOMENDACIÓN A FUTURO: moler la cáscara de huevo hasta una granulometría tipo polvo.

![](../images/TI/huevo1.png){ width=50% }![](../images/TI/huevo2.png){ width=50% }

###CONCLUSIÓN DE BIOMATERIALES

**Consideramos mejor estandarizar la mezcla y luego modificar parámetros de impresión, ya que es más fácil trabajar con una mezcla constante (receta) y luego modificar parámetros en Cura.**

##SOLDADO DE PLACAS

La segunda práctica del taller consistió de soldar los componentes a una placa que habíamos diseñado para una consigna anterior (MT08).

![](../images/TI/placa.png){ width=50% }![](../images/TI/soldando.png){ width=50% }

```

Comentario: Fue mi primera experiencia soldando e imprimiendo biomateriales. Lo ideal hubiera sido tener
más tiempo para estandarizar los materiales de impresión
para luego poder dedicar tiempo a los parámetros de impresión.

```
