---
hide:
    - toc
---

# <span style="color:green"> MI
## <span style="color:green"> Diseño computacional

<span style="color:green"> WORKSHOP de Arturo De La Fuente

**1.Diseño ALGORITMICO**

La información se va construyendo en una lista

**2.Diseño GENERATIVO**

Permite a partir de un código generar iteraciones (cambio en una variable) del mismo modelo.

![](../images/MI/generativo.png){ width=30% }

**3.Diseño ASOCIATIVO**

Variables asociadadas entre si.

**4.Diseño RESPONSIVO**

Puede diseñarse una vez en base a parámetros o responsivo a mecánica que responde a sistemas con motores, como fachadas a factores climáticos.

**5.Diseño RECURSIVO/ITERATIVO:**

Generar iteraciones una tras otra, puede ser en forma de loop (recursivo). Genera complejidades que no serían sencillas de hacer.

![](../images/MI/fractal.jpeg){ width=30% }

**6.Diseño GENETICO-EVOLUTIVO:**

Teoría de selección natural: encontrar soluciones a problemas que a través de iteraciones en el tiempo van encontrando la evolución de la especie hasta llegar a un resultado óptimo. Resuelven los problemas numéricamente.

**7.Diseño EMERGENTE:**

Comportamiento emergente de los cardúmenes por sobrevivir.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TzpVx2o_4Lw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**8.Diseño BIOMIMÉTICO**

**9.Diseño RETROALIMENTATIVO**

**10.Diseño INTELIGENTE**

**11.Diseño FORM FINDING**

**12.Diseño COMPUTACIONAL**

**13.Diseño EFICIENTE**

***

### <span style="color:green"> Consigna

**La consigna de este modulo consistió en:**

*Elegir uno de los ejercicios en clase, editarlo y documentar en nuestra página web*

A partir del [Archivo .gh](https://gitlab.com/valentina_roel/valeroel/-/blob/master/docs/files/image%20sampler.gh) de Arturo decidí crear el modelo para una escultura de mi departamento (Treinta y Tres).

En QGIS hice un mapa de relieve en b&w para poner como imagen.

![](../images/MI/TyTres.png){ width=50% }

Modificando los numer sliders llegué al sigiente diseño:

![](../images/MI/intento.png){ width=50% }

Luego con ayuda de Arturo se le realzó un off set a cada cuadrado de forma de lograr un efecto similar al que tiene el Holocaust-Mahnmal. De esta forma los visitantes podrían ingresar caminando a través de la escultura.

![](../images/MI/correccion.png){ width=50% }

Obteniendo el diseño final:

![](../images/MI/final.png){ width=50% }
![](../images/MI/perspectiva_final.png){ width=50% }

[Archivo .gh](https://gitlab.com/valentina_roel/valeroel/-/blob/master/docs/files/TyT_image%20sampler.gh)
