---
hide:
    - toc
---

# <span style="color:green"> MI
## <span style="color:green"> Innovación abierta



## CLASE 1

### CONCEPTO CLAVE

La **Innovación abierta** es:

Una nueva estrategia de innovación mediante la cual las empresas van más allá de sus límites y desarrollan la cooperación con organizaciones o profesionales externos (Henry Chesbrough).

-Rápida.

-Económico: es un proceso más barato por ser abierto a la comunidad.

-Consciencia: de que la comunidad tiene las respuestas, no las empresas.

-Conocimiento distribuido en red.

### PROCESO GENÉRICO DE LA INNOVACIÓN

![](../images/MI2/innovacion.jpeg){ width=50% }

### 10 PUNTOS CLAVE DE LA INNOVACIÓN ABIERTA

1.  **Compromiso de la dirección:** la innovación solo tiene sentido si se aplica a largo plazo.
2.  **Participación de la dirección**
3.  **La información es clave:** se debe compartir para que avance rápido.
4.  **El objetivo es el conocimiento:** la clave es saber algo nuevo para aplicar una mejora.
5.  **Tan importante como la organización son los trabajadores:** el equipo es quien lo lleva a cabo. Los trabajadores se deben de sentir parte.
6.  **Gestionar la responsabilidad que implica la libertad:** es una oportunidad en un proceso global, la responsabilidad no implica castigo, sino gestión del error.
7.  **El error forma parte de la gestión:** se utiliza para aprender.
8.  **Redarquía:** apunta a la mayor horizontalidad posible y transferencia de la información lo más rápida posible.
9.  **Compartir información es bueno:**
10. **La innovación abierta no es una metodología:** no es un proceso pautado que se implanta de igual manera en todas las empresas.

### REGLAS CLARAS PARA LA INNOVACIÓN ABIERTA

-   **El problema o la pregunta que nos ocupa** se debe de poder resumir en un tweet.
-   **Los incentivos disponibles:** deben ser claros para todos.
-   **Expectativas de propiedad intelectual:** clave que los participantes lo tengan claro.
-   **Como y cuando las personas deben enviar ideas:**

### MODELOS COMUNES DE INNOVACIÓN ABIERTA

1.  Desafíos: por ejemplo 24 horas de innovación
2.  Asociaciones de empresas emergentes
3.  Hackatones
4.  Laboratorios de co-creación

***

## CLASE 2


### CONCEPTO CLAVE

**Open Source** es: una expresión que quiere decir fuente abierta. Comenzó con el Software, el usuario tiene acceso al código de programación, lo que permite modificaciones por parte del usuario.


Es diferente el Open Source del Software libre (puede descargarse y distribuirse de manera gratuita), este ultimo puede no brindar acceso al código.

### Open Software

![](../images/MI2/opensoft.png)

Software cuyo código fuente se ha puesto a disposición de todo el mundo de manera gratuita y otorgado con licencias que facilita su reutilización o adaptación a contextos diferentes.

1.  Libertad de usar el programa con cualquier propósito
2.  Distribuir copias para ayudar a otros usuarios
3.  Realizar modificaciones y publicarlas para beneficio de otras personas.

### Open Hardware

![](../images/MI2/openhard.jpeg){ width=30% }


Se refiere a la disponibilidad del diseño para que cualquiera pueda estudiarlo, modificarlo y distribuirlo, además de poder producir y vender hardware basado en ese diseño.



### LINKS DE INTERÉS:

[Video Impresora Mendel]()

[Introducing the Open Compute Project]()


**Open Source Software:**

[GitLab]()

[SourceForage]()

[Cloud Source Repositories]()

[GitKraken]()

[Apache Allura]()


**Open Source Hardware:**

[Listado de proyectos de Wikipedia]()

[GitLab]()

[Open Harware Repository]()

[OpenSource.com/Hardware]()

[Instructables]()

***
