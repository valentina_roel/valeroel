---
hide:
    - toc
---


# <span style="color:green"> Idea y Concepto

![](../images/PROYECTO/slide.png)

1. IDEA Y CONCEPTO DE PROYECTO
Se desarrollará un sistema de control de aves para chacras agrícolas.
Funciona con un sensor de movimiento RCWL-0516 que emite una respuesta de sonido para ahuyentar las aves.

2. AUDIENCIA DEL PROYECTO
Quienes aplicarán dicha tecnología serán productores agrícolas, tanto mujeres como hombres que desean disminuir la brecha de rendimiento ocasionada por los ataques de aves previos a la cosecha. Se utilizará en parcelas experimentales principalmente ya que el costo de lo que se está produciendo es mayor que en una chacra con fines productivos.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/733013585?h=566fb6ec74&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Presentaci&amp;oacute;n final UTEC.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

[Slide](https://gitlab.com/valentina_roel/valeroel/-/raw/master/docs/images/PROYECTO/slide.png?inline=false)
[Video](https://gitlab.com/valentina_roel/valeroel/-/blob/master/docs/images/PROYECTO/Presentación%20final%20UTEC.mp4)
