---
hide:
    - toc
---

# Propuesta final

Luego de tener la idea definida del proyecto se comenzó con el isotipo del mismo.

![](../images/isotipo.svg){ width=25% }

En el módulo de impresión 3D se realizó una caja para contener la placa Arduino en el campo en [Rhinoceros®](y https://www.rhino3d.com).

![](../images/MT05/Rhino/impresion.jpeg){ width=50% }

También se trabajó en [Fusion360](https://www.autodesk.com/products/fusion-360/personal) realizando un diseño a partir de un modelo del repositorio de [Fusion360](https://www.autodesk.com/products/fusion-360/personal).

![](../images/MT04/final.png){ width=50% }

Para la versión final de la caja se calcularon las medidas de la placa y sensor a colocar de forma diseñar una superficie para que queden fijos en la misma.

![](../images/PROYECTO/fijacion.jpg){ width=50% }


A su vez, se diseñó una tercera pieza (agarradera) para ajustar la caja al caño de PVC que hace de soporte en el campo.

Y se diseñaron tornillos para ajustar ambas tapas de la caja a la agarradera.

![](../images/PROYECTO/caja.jpg){ width=50% } ![](../images/PROYECTO/caja2.jpg){ width=50% }

Agarradera y tornillos [.3dm](https://gitlab.com/valentina_roel/valeroel/-/raw/master/docs/files/agarradera%20y%20tornillos.3dm)

Caja [.3dm](https://gitlab.com/valentina_roel/valeroel/-/commit/aeb4a845d803d14d7b5822a30716713672599e12)
