---
hide:
    - toc
---

# <span style="color:green"> Desarrollo del proyecto

## RCWL-0516 Microwave Proximity Sensor

Este módulo de sensor de radar mini doppler está equipado con un voltaje amplio de 4-28 V DC. 360 grados sin detección de ángulo ciego y distancia máxima de detección de 7 m aproximadamente con tiempo de retardo y sensibilidad ajustables. Este módulo ha sido diseñado como una alternativa a los detectores de movimiento PIR. Es perfecto para el interruptor de luz con sensor de movimiento de microondas DIY, juguetes con sensores humanos, dispositivos de seguridad inteligentes, etc.
Puede utilizarse dentro de una caja plástica sin problemas, debiendo mantener una distacia con la placa de Arduino ya que los metales causan interferencia en la detección.

![](/Users/valentinaroel/efdia/valeroel/docs/images/PROYECTO/RCWL.png){ width=70% }

Se probó el sensor con dos códigos diferentes. Resta soldar los pines al sensor y probar con un BUZZER

### PRUEBA 1
```
#define Sensor 2

void setup() {
  // put your setup code here, to run once:
  pinMode (Sensor, INPUT);
  Serial.begin (9600);
}

void loop() {
  // put your main code here, to run repeatedly:
bool Detection = digitalRead(Sensor);

if(Detection == HIGH)
Serial.println("Movimiento");
if(Detection == LOW)
Serial.println("Libre");

}

```
Se puede observar que es extremadamente sensible al momento de probarlo.

![](../images/PROYECTO/sensor.jpeg){ width=40% }
![](../images/PROYECTO/monitor_serie.png){ width=40% }

### PRUEBA 2

La siguiente prueba es más similar a como será el prototipo del proyecto. En este caso la respuesta es el encendido de un LED por no disponer de un Buzzer al momento.

```
#define Sensor 2
#define LED 3

void setup() {

pinMode(Sensor,INPUT);
pinMode(LED,OUTPUT);

}

void loop() {

bool Detection = digitalRead(Sensor);

if(Detection == HIGH)
digitalWrite(LED,HIGH);
if(Detection == LOW)
digitalWrite(LED,LOW);

}

```

<iframe src="https://player.vimeo.com/video/710141325?h=16833eda29&amp;title=0&amp;byline=0&amp;portrait=0&amp;speed=0&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="400" height="300" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="LED.mp4"></iframe>

El código utilizado en el proyecto final es el siguiente:
Se utiliza el *monitor serie* con el fin de poder observar si el el sensor funciona y las conexiones están bien hechas.

```
int Sensor = 11;     // RCWL-0516 Input Pin
int Buzzer = 13;       // BUZZER Output Pin

int sensorval = 0;  // RCWL-0516 Sensor Value



void setup() {
  pinMode (Sensor, INPUT);  // RCWL-0516 as input
  pinMode (Buzzer, OUTPUT);    // buzzer as OUTPUT
  digitalWrite(Buzzer, LOW);   // Turn buzzer Off

  Serial.begin(9600);  //para probar conectado a la PC

  Serial.println("Waiting for motion");  //para probar conectado a la PC
}


void loop(){

  sensorval = digitalRead(Sensor);  // Read Sensor value

  if (sensorval == HIGH) {        
    digitalWrite(Buzzer, HIGH);  // Turn buzzer On
    Serial.println("Motion Detected");  // para probar conectado a la PC
  }

  if (sensorval == LOW) {        
    digitalWrite(Buzzer, LOW);  // Turn buzzer Off
  }
}
```

Se realiza una prueba cerrando la caja con tornillos metálicos y ajustando la caja con una cinta metálica.

![](../images/PROYECTO/metalico.JPG){ width=49% } ![](../images/PROYECTO/metalico2.jpg){ width=49% }

Los componentes metálicos realizan mucha interferencia con el sensor por lo que se diseñan los tornillos y una pieza para ajustar la caja al soporte de PVC que irá en el campo.

![](../images/PROYECTO/metalvsplastico.jpg){ width=50% }

Los tornillos son modelos de Fusion360 que fueron modificados y escalados es Rhino para alcanzar las medidas necesarias.

![](../images/PROYECTO/tornillo.png){ width=50% }

A la agarradera se le diseña la rosca interior para evitar tener que imprimir más piezas que serían las tuercas, la rosca ya está incluida dentro de la misma pieza.

![](../images/PROYECTO/agarradera.png){ width=50% }

Por lo tanto al colocar los tornillos a través de la caja y alineados con la agarradera se enroscan y cierran la pieza herméticamente.

La agarradera fue diseñada con precisión para que calzara perfectamente con el soporte de PVC, teniendo que sujetar el peso de la caja y evitar su deslizamiento.

![](../images/PROYECTO/soporte.jpg){ width=50% }


### MONTAJE A CAMPO

Pasos a seguir:

1.  Para colocar el dispositivo de manera rápida en el campo conviene tener los soportes de PVC ya instalados estratégicamente.

2.  PowerBank o panel solar con batería cargada (fuente energética).

3.  Cerrar la caja y ajustar al soporte de PVC usando la agarradera.

4.  Conectar la placa Arduino a la fuente energética.

![](../images/PROYECTO/montaje.jpeg){ width=48% } ![](../images/PROYECTO/montaje2.jpeg){ width=48% }
